import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import PhotoCard from './photo-card';
import Button from '../../components/button';
import './style.scss';

/**
 * A list component to display the Photos as a grid.
 * The component was decoupled from {@link EndoubleReactAssignment} in order to
 * have a component focus on rendering the photo list instead of having to fetch
 * for the data itself.
 *
 * The component displays a list of `4` Photos per row and as you reduce the size
 * of the page/browser the list gets shorter by `1` Photo until there's only rows
 * of `1` displayed Photo Cards.
 *
 * The component is composed by an array of `PhotoCards` to render the Photo
 * thumbnails indivually.
 *
 * This component complies with the Presentational and Container pattern.
 */
class PhotoList extends PureComponent {
    static propTypes = {
        /**
         * Boolean indicating if there's a load/fetching process going on
         */
        isLoading: PropTypes.bool.isRequired,

        /**
         * Function to fetch the next page of the Photo List.
         * Passed down to the `Load More` button.
         */
        loadMorePhotos: PropTypes.func.isRequired,

        /**
         * Open {@link PhotoDetails} window when a thumbnail is clicked
         */
        openDetailsWindow: PropTypes.func.isRequired,

        /**
         * Photo list to be rendered as a {@link PhotoCard}
         */
        photos: PropTypes.arrayOf(PropTypes.shape({
            author: PropTypes.string.isRequired,
            id: PropTypes.string.isRequired,
            thumbnail: PropTypes.string.isRequired,
        })).isRequired,
    }

    /**
     * Render each photo thumbnail using {@link PhotoCard}
     */
    renderPhotoCards() {
        const { openDetailsWindow, photos } = this.props;

        return photos.map((photo, index) => (
            <PhotoCard
                {...photo}
                className="photo-list__card"
                key={photo.id}
                tabIndex={index + 1}
                onClick={openDetailsWindow}
            />
        ));
    }

    render() {
        const { loadMorePhotos } = this.props;

        return (
            <div className="photo-list">
                <div className="photo-list__grid">
                    {this.renderPhotoCards()}
                </div>
                <div className="photo-list__footer">
                    <Button onClick={loadMorePhotos}>Load More</Button>
                </div>
            </div>
        );
    }
}

export default PhotoList;
