import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Constants from '../../../utils/constants';

import './style.scss';

/**
 * Photo thumbnail used to render the photo summary in {@link PhotoList}.
 * It displayes the Photo thumbnail, the description given by the author and the author's name
 */
class PhotoCard extends PureComponent {
    static propTypes = {
        /**
         * Author's name.
         */
        author: PropTypes.string.isRequired,

        /**
         * An optional className to apply to the entire card.
         */
        className: PropTypes.string,

        /**
         * A function to call when the `click` event is triggered over the entired card.
         */
        onClick: PropTypes.func.isRequired,

        /**
         * The photography ID.
         */
        id: PropTypes.string.isRequired,

        /*
         * The tab index to use so it is keyboard focusable.
         */
        tabIndex: PropTypes.number.isRequired,

        /**
         * The photo thumbnail.
         */
        thumbnail: PropTypes.string.isRequired,
    }

    static defaultProps = {
        className: '',
    }

    openDetailsWindow = () => {
        const { id, onClick } = this.props;
        onClick(id);
    }

    handleKeyPressed = (e) => {
        if ((e.which || e.keyCode) === Constants.keyCodes.ENTER) {
            this.openDetailsWindow();
        }
    }

    render() {
        const {
            author,
            className,
            tabIndex,
            thumbnail,
        } = this.props;

        return (
            <div
                className={classNames('photo-card', className)}
                onClick={this.openDetailsWindow}
                onKeyPress={this.handleKeyPressed}
                role="button"
                tabIndex={tabIndex}
            >
                <div className="photo-card__thumbnail-container">
                    <img className="photo-card__thumbnail" src={thumbnail} alt="" />
                </div>
                <div className="photo-card__footer">
                    <span className="photo-card__caption">
                        {`by ${author}`}
                        {/* the backtick is used due the eslint rule react/jsx-one-expression-per-line */}
                    </span>
                </div>
            </div>
        );
    }
}

export default PhotoCard;
