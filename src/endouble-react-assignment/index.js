import React, { Component } from 'react';

import Filters from './filters';
import PhotoList from './photo-list';
import PhotoDetails from './photo-details';
import PhotoHelpers from '../utils/photo-helpers';
import { getPhotoList, getPhotoDetails } from '../utils/api';

import './style.scss';

/**
 * The `EndoubleReactAssignment` component is the main container built present the main skeleton of
 * the application and to decouple the logic and API requests from the rendered elements themselves.
 *
 * This component complies with the Presentational and Container pattern.
 */
class EndoubleReactAssignment extends Component {
    state = {
        activePhotoDetails: undefined,
        error: undefined,
        filterParameter: '',
        filteredPhotos: [],
        isLoading: true,
        photoListCurrentPage: 1,
        photos: [],
        showDetailsWindow: false,
    }

    componentDidMount() {
        this.loadPhotosPerPage();
    }

    /**
     * IMPORTANT NOTE
     */
    // All functions bellow were created using Babel's class-property.
    // This to maintain the context (via "Closure") when passed down to a Component
    // instead of using `.bind(this)` in the constructor or creating a closure every
    // time in the render method every time a render event occurs, which could launch
    // a re-render event.

    /**
     * Close the details window in a "controlled" way.
     */
    closeDetailsWindow = () => {
        this.setState({
            activePhotoDetails: undefined,
            showDetailsWindow: false,
        });
    }

    /**
     * Open the details window in a "controlled" way.
     */
    openDetailsWindow = (photoId) => {
        getPhotoDetails(photoId)
            .then((activePhotoDetails) => {
                this.setState({
                    activePhotoDetails,
                    showDetailsWindow: true,
                });
            });
    }

    /**
     * Update the filter parameter and updated the photo list copy to
     * render the new filtered list.
     * Both lists must be maintained, the orignal (full) and the filtered (partial).
     * The filtered list is the one to be rendered and the original to have a source to filter
     */
    changeFilterParameters = (newFilterValue) => {
        const { photos } = this.state;

        this.setState({
            filterParameter: newFilterValue,
            filteredPhotos: PhotoHelpers.filterPhotoList(photos, newFilterValue),
        });
    }

    /**
     * Fetch the photo summary list from the endpoint by page
     */
    loadPhotosPerPage = () => {
        const { filterParameter, photos, photoListCurrentPage } = this.state;

        getPhotoList(photoListCurrentPage)
            .then(newPhotos => this.setState({
                error: undefined,
                filteredPhotos: PhotoHelpers.filterPhotoList([...photos, ...newPhotos], filterParameter),
                isLoading: false,
                photoListCurrentPage: photoListCurrentPage + 1,
                photos: [...photos, ...newPhotos],
            }))
            .catch(() => this.setState({
                error: 'Ups... an error',
                filteredPhotos: [],
                isLoading: false,
                photoListCurrentPage: 1,
                photos: [],
            }));
    }

    render() {
        const {
            activePhotoDetails,
            error,
            isLoading,
            filteredPhotos,
            showDetailsWindow,
        } = this.state;

        if (error) {
            return error;
        }

        return (
            <div className="endouble-react-assignment">
                <header className="endouble-react-assignment__header">
                    <h1 className="endouble-react-assignment__header-text">Endouble React Assignment</h1>
                </header>
                <Filters
                    changeFilterParameters={this.changeFilterParameters}
                />
                <PhotoList
                    isLoading={isLoading}
                    loadMorePhotos={this.loadPhotosPerPage}
                    openDetailsWindow={this.openDetailsWindow}
                    photos={filteredPhotos}
                />
                <PhotoDetails
                    onHide={this.closeDetailsWindow}
                    photo={activePhotoDetails}
                    visible={showDetailsWindow}
                />
            </div>
        );
    }
}

export default EndoubleReactAssignment;
