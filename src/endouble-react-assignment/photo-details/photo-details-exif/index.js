import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const PhotoDetailsExif = ({
    aperture,
    brand,
    exposureTime,
    focalLength,
    iso,
    model,
}) => (
    <div className="photo-details-exif">
        <div className="photo-details-exif__maker">
            <i className="material-icons photo-details-exif__camera-icon" title="Camera">camera_alt</i>
            <div>
                <span className="photo-details-exif__item">{brand}</span>
                <span className="photo-details-exif__item">{model}</span>
            </div>
        </div>

        <div className="photo-details-exif__specs">
            <div className="photo-details-exif__item">
                <i className="material-icons photo-details-exif__icon" title="Focal Length">straighten</i>
                <span>
                    {focalLength}
                </span>
            </div>

            <div className="photo-details-exif__item">
                <i className="material-icons photo-details-exif__icon" title="Focal length">camera</i>
                <span>
                    {aperture}
                </span>
            </div>

            <div className="photo-details-exif__item">
                <i className="material-icons photo-details-exif__icon" title="Exposure time">timer</i>
                <span>
                    {exposureTime}
                </span>
            </div>

            <div className="photo-details-exif__item">
                <i className="material-icons photo-details-exif__icon" title="ISO">iso</i>
                <span>
                    {iso}
                </span>
            </div>
        </div>
    </div>
);

PhotoDetailsExif.propTypes = {
    aperture: PropTypes.string.isRequired,
    brand: PropTypes.string.isRequired,
    exposureTime: PropTypes.string.isRequired,
    focalLength: PropTypes.string.isRequired,
    iso: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]).isRequired,
    model: PropTypes.string.isRequired,
};

export default PhotoDetailsExif;
