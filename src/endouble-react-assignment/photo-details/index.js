import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Modal from '../../components/modal';
import PhotoDetailsExif from './photo-details-exif';

import './style.scss';

class PhotoDetails extends PureComponent {
    static propTypes = {
        /**
        * A function to call that will close the Photo Details modal
        */
        onHide: PropTypes.func.isRequired,

        /**
        * Object containing the image details
        */
        photo: PropTypes.shape({
            author: PropTypes.shape({
                name: PropTypes.string.isRequired,
                profile: PropTypes.string.isRequired,
            }),
            description: PropTypes.string.isRequired,
            exif: PropTypes.shape({
                aperture: PropTypes.string.isRequired,
                brand: PropTypes.string.isRequired,
                exposureTime: PropTypes.string.isRequired,
                focalLength: PropTypes.string.isRequired,
                iso: PropTypes.oneOfType([
                    PropTypes.number,
                    PropTypes.string,
                ]).isRequired,
                model: PropTypes.string.isRequired,
            }),
            id: PropTypes.string.isRequired,
            image: PropTypes.string.isRequired,
        }),

        /**
        * Boolean if the Photo Details modal is current visible.
        */
        visible: PropTypes.bool.isRequired,
    }

    static defaultProps = {
        photo: undefined,
    }

    render() {
        const { onHide, photo, visible } = this.props;

        if (!visible) {
            return null;
        }

        return (
            <Modal
                className="photo-details"
                onHide={onHide}
                visible={visible}
            >
                <div>
                    <a className="photo-details__photographer" href={photo.author.profile}>{photo.author.name}</a>
                    <br />
                    <span className="photo-details__description">{photo.description}</span>
                </div>

                <hr className="photo-details__separator" />

                <PhotoDetailsExif {...photo.exif} />

                <hr className="photo-details__separator" />

                <div className="photo-details__image-container">
                    <img className="photo-details__image" src={photo.image} alt={photo.description} />
                </div>
            </Modal>
        );
    }
}

export default PhotoDetails;
