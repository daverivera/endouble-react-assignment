import React from 'react';
import PropTypes from 'prop-types';

import TextField from '../../components/text-field';

import './style.scss';

const Filters = ({ changeFilterParameters }) => (
    <div className="filters">
        <TextField
            onChange={changeFilterParameters}
            placeholder="Photographer's name"
        />
    </div>
);

Filters.propTypes = {
    /**
     * A function to call when the value changes in the text field,
     * this will be called with the new text field's value and
     * the change event.
     *
     * ```js
     * onChange(e.target.value, e);
     * ```
     */
    changeFilterParameters: PropTypes.func.isRequired,
};

export default Filters;
