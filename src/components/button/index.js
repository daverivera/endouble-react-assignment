import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

/**
 * Generic `Button` component.
 * This is an styled button to maintain the same look and feel everywhere on the application.
 */
const Button = ({ children, onClick }) => (
    <button
        className="button"
        onClick={onClick}
        type="button"
    >
        {children}
    </button>
);

Button.propTypes = {
    /**
     * An optional content to be displayed within the button.
     */
    children: PropTypes.node,

    /**
     * An optional function to call when the `click` event is triggered.
     */
    onClick: PropTypes.func,
};

Button.defaultProps = {
    children: null,
    onClick: () => {},
};

export default Button;
