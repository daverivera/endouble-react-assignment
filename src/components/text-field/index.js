import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './style.scss';

class TextField extends PureComponent {
    static propTypes = {
        /**
         * A function to call when the value changes in the text field,
         * this will be called with the new text field's value and
         * the change event.
         *
         * ```js
         * onChange(e.target.value, e);
         * ```
         */
        onChange: PropTypes.func,


        /**
         * An optional placeholder text to display in the text field.
         */
        placeholder: PropTypes.string,
    }

    static defaultProps = {
        onChange: () => {},
        placeholder: undefined,
    }

    changeFilterValue = (e) => {
        const { onChange } = this.props;
        if (onChange) {
            onChange(e.target.value, e);
        }
    }

    render() {
        const { placeholder } = this.props;

        return (
            <input
                className="text-field"
                onChange={this.changeFilterValue}
                placeholder={placeholder}
                type="text"
            />
        );
    }
}

export default TextField;
