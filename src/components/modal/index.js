import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Constants from '../../utils/constants';

import './style.scss';

/**
 *  Modal is a static component for creating dialogs positioned over everything else
 *  in the document.
 */
class Modal extends PureComponent {
    static propTypes = {
        /**
        * Content to be displayed within the modal once open.
        */
        children: PropTypes.node.isRequired,

        /**
        * An optional className to apply to the modal's content.
        */
        className: PropTypes.string,

        /**
        * A function to call that will close the dialog.
        */
        onHide: PropTypes.func.isRequired,

        /**
         * Boolean if the Dialog is current visible.
         */
        visible: PropTypes.bool.isRequired,
    }

    static defaultProps = {
        className: '',
    }

    componentDidMount() {
        // Disable linter to add the event and close the window when ESC is pressed.
        // Adding the event to the `ref` won't work.
        // eslint-disable-next-line no-undef
        window.addEventListener('keydown', this.handleEscClose.bind(this));
    }

    /**
     * Verifying if the pressed key is Esc and close modal
     * @param e - Event object
     */
    handleEscClose(e) {
        const { onHide } = this.props;

        if ((e.which || e.keyCode) === Constants.keyCodes.ESC) {
            onHide(e);
        }
    }

    render() {
        const {
            children,
            className,
            onHide,
            visible,
        } = this.props;

        return (
            <div className={classNames('modal', { 'modal--visible': visible })}>
                <div className="modal__background" />
                <div className="modal__content">
                    <div className="modal__box">
                        <div className="modal__content-header">
                            <i
                                className="material-icons modal__close-icon"
                                onClick={onHide}
                                role="button"
                                tabIndex={0}
                                onKeyPress={e => this.handleEscClose(e)}
                            >
                                close
                            </i>
                        </div>

                        <div className={className}>
                            {children}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;
