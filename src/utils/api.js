import axios from 'axios';

import Constants from './constants';
import PhotoHelpers from './photo-helpers';

const { REACT_APP_UNSPLASH_ACCESS_KEY } = process.env;

/**
 * Interecepting every HTTP request to inject Unsplash's Access Key to the headers.
 */
axios.interceptors.request.use(config => Object.assign(config, {
    headers: {
        Authorization: `Client-ID ${REACT_APP_UNSPLASH_ACCESS_KEY}`,
    },
}));

/**
 * Get the paginated Photo List from Unsplash fetching 10 photos per request.
 * The list is processed by {@link PhotoHelpers.processPhotoList} helper to transform the data.
 * @param {number} - Page number.
 * @return {Promise} - Promise to control the flow of the request (successful/failed).
 *                     The promise represents the photo list.
 */
export const getPhotoList = (page = 1) => axios(
    Constants.unsplashApiEndpoints.photos,
    { params: { page } },
)
    // Then's first class function has been decoupled and injected to allow reusability and testability of the function
    .then(({ data }) => PhotoHelpers.processPhotoList(data));

export const getPhotoDetails = photoId => axios(`${Constants.unsplashApiEndpoints.photos}/${photoId}`)
    .then(({ data }) => PhotoHelpers.processPhotoDetails(data));
