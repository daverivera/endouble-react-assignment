import PhotoHelpers from '../photo-helpers';

describe('Photo Helpers', () => {
    describe('processPhotoList', () => {
        describe('when receiving the photo list from the API', () => {
            test('should parse the data and remove the unnecessary fields', () => {
                const photoList = [
                    {
                        id: 'f3k31d',
                        created_at: '2018-11-21T17:18:39-05:00',
                        updated_at: '2019-02-06T11:59:52-05:00',
                        width: 1729,
                        description: 'Deep thought photography quote',
                        urls: {
                            thumb: 'http://fakephotographyserver.com/photo/:id.jpg',
                        },
                        user: {
                            name: 'George Lucas',
                        },
                    },
                ];

                expect(PhotoHelpers.processPhotoList(photoList))
                    .toEqual([{
                        author: 'George Lucas',
                        id: 'f3k31d',
                        thumbnail: 'http://fakephotographyserver.com/photo/:id.jpg',
                    }]);
            });
        });

        describe('when the photo list is empty', () => {
            test('should return an empty list', () => {
                const photoList = [];

                expect(PhotoHelpers.processPhotoList(photoList))
                    .toHaveLength(0);
            });
        });
    });

    describe('processPhotoDetails', () => {
        describe('when receiving the photo details from the API with every field set', () => {
            test('should parse the data and remove the unnecessary fields', () => {
                const photoDetails = {
                    description: 'Deep thought photography quote',
                    exif: {
                        make: 'Canon',
                        model: 'Canon EOS 5D Mark IV',
                        exposure_time: '1/800',
                        aperture: '3.2',
                        focal_length: '35.0',
                        iso: 200,
                    },
                    id: 'f3k31d',
                    user: {
                        name: 'George Lucas',
                        links: {
                            html: 'https://unsplash.com/@GeorgeLucasILM',
                        },
                    },
                    urls: {
                        small: 'http://fakephotographyserver.com/photo/:id.jpg',
                    },
                };

                expect(PhotoHelpers.processPhotoDetails(photoDetails))
                    .toEqual({
                        author: {
                            name: 'George Lucas',
                            profile: 'https://unsplash.com/@GeorgeLucasILM',
                        },
                        description: 'Deep thought photography quote',
                        exif: {
                            aperture: '3.2',
                            brand: 'Canon',
                            exposureTime: '1/800',
                            focalLength: '35.0',
                            iso: 200,
                            model: 'Canon EOS 5D Mark IV',
                        },
                        id: 'f3k31d',
                        image: 'http://fakephotographyserver.com/photo/:id.jpg',
                    });
            });
        });

        describe('when receiving the photo details from the API with some empty fields', () => {
            test('should parse the data and remove the unnecessary fields', () => {
                const photoDetails = {
                    description: null,
                    exif: {
                        make: null,
                        model: null,
                        exposure_time: null,
                        aperture: null,
                        focal_length: null,
                        iso: null,
                    },
                    id: 'f3k31d',
                    user: {
                        name: 'George Lucas',
                        links: {
                            html: 'https://unsplash.com/@GeorgeLucasILM',
                        },
                    },
                    urls: {
                        small: 'http://fakephotographyserver.com/photo/:id.jpg',
                    },
                };

                expect(PhotoHelpers.processPhotoDetails(photoDetails))
                    .toEqual({
                        author: {
                            name: 'George Lucas',
                            profile: 'https://unsplash.com/@GeorgeLucasILM',
                        },
                        description: '',
                        exif: {
                            aperture: '--',
                            brand: '--',
                            exposureTime: '--',
                            focalLength: '--',
                            iso: '--',
                            model: '--',
                        },
                        id: 'f3k31d',
                        image: 'http://fakephotographyserver.com/photo/:id.jpg',
                    });
            });
        });
    });

    describe('filterPhotoList', () => {
        describe('when filtering the photo list', () => {
            test('should filter the list by author name containing the filter paramter and return a new list', () => {
                const photoList = [
                    { id: 'qRs4h6eRhNI', author: 'Kyle Loftus' },
                    { id: 'hZQOby6ZdIE', author: 'Rutz Shepp' },
                    { id: 'bJA4g7-h9E4', author: 'Eiliv-Sonas Aceron' },
                    { id: '1Q9whdktZNU', author: 'Christopher Rusev' },
                    { id: 'Lq4YDLtFUYI', author: 'Should Wang' },
                    { id: 'xhB9Pf4IfMQ', author: 'Should Wang' },
                ];

                const filterParameter = 'SH';

                expect(PhotoHelpers.filterPhotoList(photoList, filterParameter))
                    .toEqual([
                        { id: 'hZQOby6ZdIE', author: 'Rutz Shepp' },
                        { id: 'Lq4YDLtFUYI', author: 'Should Wang' },
                        { id: 'xhB9Pf4IfMQ', author: 'Should Wang' },
                    ]);

                // Verify function is pure
                expect(photoList).toHaveLength(6);
                expect(filterParameter).toEqual('SH');
            });
        });

        describe('when filtering the photo list with an empty filter paramter', () => {
            test('should return the same list', () => {
                const photoList = [
                    { id: 'qRs4h6eRhNI', author: 'Kyle Loftus' },
                    { id: 'hZQOby6ZdIE', author: 'Rutz Shepp' },
                    { id: 'bJA4g7-h9E4', author: 'Eiliv-Sonas Aceron' },
                    { id: '1Q9whdktZNU', author: 'Christopher Rusev' },
                    { id: 'Lq4YDLtFUYI', author: 'Should Wang' },
                    { id: 'xhB9Pf4IfMQ', author: 'Should Wang' },
                ];

                const filterParameter = '';

                expect(PhotoHelpers.filterPhotoList([...photoList], filterParameter))
                    .toEqual(photoList);
            });
        });
    });
});
