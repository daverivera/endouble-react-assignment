const EMPTY_DETAIL_VALUE = '--';

/**
 * Process the photo list to only return what's really needed from the entire object.
 * This to reduce the amount of data store and to imply what values are actually
 * being used by the entire application.
 *
 * @param {obejct} photoList - List of photo summaries returned by the API
 * @returns {object} Summary of the entire list with only values used by the application
 */
const processPhotoList = photoList => photoList
    .map(({ id, urls, user }) => ({ author: user.name, id, thumbnail: urls.thumb }));

/**
 * Process the entire photo details to only return what's really needed from the entire object
 * and to parse the data into what needs to be displayed on the components.
 * This is to aviod doing it on the component itself which would cause a re-render event.
 *
 * @param {obejct} Photo - Photo details containing the EXIF information
 * @returns {object} Photo details with only the neceessary fields and the data parsed ready to be rendered
 */
const processPhotoDetails = ({
    description,
    exif,
    id,
    user,
    urls,
}) => ({
    author: {
        name: user.name,
        profile: user.links.html,
    },
    description: description || '',
    exif: {
        aperture: exif.aperture || EMPTY_DETAIL_VALUE,
        brand: exif.make || EMPTY_DETAIL_VALUE,
        exposureTime: exif.exposure_time || EMPTY_DETAIL_VALUE,
        focalLength: exif.focal_length || EMPTY_DETAIL_VALUE,
        iso: exif.iso || EMPTY_DETAIL_VALUE,
        model: exif.model || EMPTY_DETAIL_VALUE,
    },
    id,
    image: urls.small,
});

const filterPhotoList = (photoList, filterParameter) => {
    if (!filterParameter) {
        return photoList;
    }

    const insensitiveFilterParameter = filterParameter.toLowerCase();

    return photoList.filter(photo => photo.author.toLowerCase().includes(insensitiveFilterParameter));
};

export default {
    filterPhotoList,
    processPhotoDetails,
    processPhotoList,
};
