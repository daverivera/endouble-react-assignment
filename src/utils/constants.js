const UNSPLASH_SERVER = 'https://api.unsplash.com';

const keyCodes = {
    ENTER: 13,
    ESC: 27,
};

const unsplashApiEndpoints = {
    photos: `${UNSPLASH_SERVER}/photos`,
};

export default {
    keyCodes,
    unsplashApiEndpoints,
};
