# A take on Endouble's React Assignment

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Architecture

The entire project's architecture is based on [Vertical Architecture](https://jimmybogard.com/vertical-slice-architecture/) in order to allocate components and their children in hierarchy to not only [show intent](https://youtu.be/Nsjsiz2A9mg?t=419) about what the application is about but to allow other developers traverse through the application. By using folders such as `components` and `containers` won't help understand the flow and relationship between the components which makes it more difficult to keep track of data from top-down and bottom-up flows.

## How to run locally

In order to run the application you must first add the ENV variable into `.env` file called: `REACT_APP_UNSPLASH_ACCESS_KEY` provided in the email or create a new one by registering in [Unsplash](https://unsplash.com/join) and create an application.

After adding the variable run:
```
npm start
```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm test:coverage`

Launches the test runner and creates the coverage of the application.
Open the `coverage/lcov-report/index.html` file in a browser to check the coverage, although coverage is returned on the console, the browser easies browsing the coverage per file.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
